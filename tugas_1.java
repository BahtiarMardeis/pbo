public class Main {
    public static void main(String[] args) {
        int[] arr = {5, 2, 9, 1, 5, 6};
        int n = arr.length;
        int minVal = arr[0];

        for (int i = 1; i < n; i++) {
            if (arr[i] < minVal) {
                minVal = arr[i];
            }
        }

        System.out.println("Nilai minimum dalam array adalah: " + minVal);
    }
}
